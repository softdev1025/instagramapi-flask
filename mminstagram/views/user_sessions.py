import uuid
from bcrypt import hashpw

from flask import flash, g, redirect, render_template, request, url_for, \
    session

from mminstagram.config import app
from mminstagram.utils import auth_required, create_user_auth_session, \
    destroy_user_auth_session, noauth_required
from mminstagram.models import Users

@app.route('/user_sessions')
@noauth_required
def new_user_session():
    email = request.args.get('email', '')
    return render_template('user_sessions/new.html', email=email)

@app.route('/user_sessions', methods=['POST'])
@noauth_required
def create_user_session():
    email = request.form.get('email')
    password = request.form.get('password')

    has_errors = False
    if not email or not len(email):
        flash('Email is required.', 'error')
        has_errors = True
    if not password or not len(password):
        flash('Password is required.', 'error')
        has_errors = True
    if has_errors:
        return redirect(url_for('new_user_session', email=email))

    try:
        user = Users.get(email=email)
    except Users.DoesNotExist:
        flash('There is no account with that email.', 'error')
        return redirect(url_for('new_user_session', email=email))

    salt = user.salt
    salted_hashed_password = hashpw(
        password.encode('utf8'),
        salt.encode('utf8'),
    )

    if salted_hashed_password != user.salted_hashed_password:
        flash('Invalid password.', 'error')
        return redirect(url_for('new_user_session', email=email))

    auth_token = str(uuid.uuid4())

    user.auth_token = auth_token
    user.save()

    create_user_auth_session(user.id, auth_token)
    
    return redirect('/')


@app.route('/user_sessions/destroy')
@auth_required
def destroy_user_session():
    email = g.current_user.email
    destroy_user_auth_session()
    flash('Logged out.', 'success')
    return redirect(url_for('new_user_session', email=email))
