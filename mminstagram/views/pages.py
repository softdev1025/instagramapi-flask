import uuid
from bcrypt import hashpw

from flask import g, redirect, url_for

from mminstagram.config import app

@app.route('/')
def index():
    if g.current_user:
        return redirect(url_for('list_instagram_accounts'))
    return redirect(url_for('new_user_registration'))
