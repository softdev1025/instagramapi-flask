import uuid

from flask import flash, redirect, render_template, request, url_for, session
from bcrypt import gensalt, hashpw

from mminstagram.models import Companies, Users
from mminstagram.utils import create_user_auth_session, noauth_required
from mminstagram.config import app
from mminstagram.slack import send_to_channel

@app.route('/user_registrations')
@noauth_required
def new_user_registration():
    name = request.args.get('name', '')
    email = request.args.get('email', '')
    company = request.args.get('company', '')
    return render_template(
        'user_registrations/new.html',
        name=name,
        email=email,
        company=company,
    )


@app.route('/user_registrations', methods=['POST'])
@noauth_required
def create_user_registration():
    name = request.form.get('name')
    email = request.form.get('email')
    company = request.form.get('company')
    password = request.form.get('password')
    password_confirmation = request.form.get('password_confirmation')

    has_errors = False
    if not name or not len(name):
        flash('Name is required.', 'error')
        has_errors = True
    if not email or not len(email):
        flash('Email is required.', 'error')
        has_errors = True
    if '.' not in email or '@' not in email:
        flash('Email is invalid.', 'error')
        has_errors = True
    if not company or not len(company):
        flash('Company is required.', 'error')
        has_errors = True
    if not password or not len(password):
        flash('Password is required.', 'error')
        has_errors = True
    if not password_confirmation or not len(password_confirmation):
        flash('Password confirmation is required.', 'error')
        has_errors = True
    if password != password_confirmation:
        flash('Password must match password confirmation.', 'error')
        has_errors = True
    if password == password.lower():
        flash('Password must have at least one uppercase letter.', 'error')
        has_errors = True
    if password == password.upper():
        flash('Password must have at least one lowercase letter.', 'error')
        has_errors = True
    if not any(char.isdigit() for char in password):
        flash('Password must have at least one number.', 'error')
        has_errors = True
    if has_errors:
        return redirect(url_for(
            'new_user_registration',
            name=name,
            email=email,
            company=company,
        ))

    try:
        user = Users.get(email=email)
        flash('Email already in use.', 'error')
        return redirect(url_for(
            'new_user_registration',
            name=name,
            email=email,
            company=company,
        ))
    except Users.DoesNotExist:
        pass

    salt = gensalt()
    salted_hashed_password = hashpw(password.encode('utf8'), salt)
    auth_token = str(uuid.uuid4())

    company = Companies.create(
        name=company
    )

    user = Users.create(
        name=name,
        email=email,
        salt=salt,
        salted_hashed_password=salted_hashed_password,
        auth_token=auth_token,
        company=company,
    )

    create_user_auth_session(user.id, auth_token)

    send_to_channel('{} created an account.'.format(email))

    flash('Created account.', 'success')
    return redirect(url_for('connect_instagram_account'))
