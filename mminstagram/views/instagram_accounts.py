from bson.objectid import ObjectId
from flask import flash, g, redirect, render_template, request, url_for

from mminstagram.config import app
from mminstagram.utils import auth_required, instagram_client, run_batch
from mminstagram.models import Companies, InstagramAccounts, Users
from mminstagram.slack import send_to_channel
from mmsensus.loaders import interests_loader, tokens_loader
from mmsensus.models.batches import Batches


@app.route('/instagram_accounts')
@auth_required
def list_instagram_accounts():
    company = g.current_user.company

    if company.instagram_accounts.count() == 0:
        return redirect(url_for('connect_instagram_account'))
    
    first_instagram_account = company.instagram_accounts[0]

    return redirect(url_for(
        'show_instagram_account',
        instagram_account_id=first_instagram_account.id
    ))


@app.route('/instagram_accounts/connect', )
@auth_required
def connect_instagram_account():
    return render_template(
        'instagram_accounts/connect.html',
        connect_url=instagram_client().get_authorize_url(scope=[
            'basic', 'public_content', 'follower_list',
        ]),
    )


@app.route('/instagram_accounts/authorize', )
@auth_required
def authorize_instagram_account():
    code = request.args.get('code')
    client = instagram_client()
    access_token, user_info = client.exchange_code_for_access_token(code)

    if not access_token:
        flash('Instagram connect failed.', 'error')
        return redirect(url_for('list_instagram_accounts'))

    company = g.current_user.company
    instagram_username = user_info['username']
    instagram_id = user_info['id']
    instagram_image_url = user_info['profile_picture']

    try:
        instagram_account = InstagramAccounts.get(id=instagram_id)
        instagram_account.access_token = access_token
        instagram_account.save()

        if instagram_account.company == g.current_user.company:
            flash(
                'You have already generated a report for that account.',
                'error',
            )
        else:
            flash('That account is already in use.', 'error')

        return redirect(url_for('list_instagram_accounts'))
    except InstagramAccounts.DoesNotExist:
        pass

    batch = Batches.create(
        inputs=[instagram_username.strip()],
        folder=ObjectId('55db28bc92cffb3a95d49cd9'),
        name='AUTOGEN: {}::{}'.format(instagram_id, instagram_username),
        service='instagram',
        batch_type='followers',
        limit=1500,
        params={
            'reclassify': False,
        },
        baseline='560341bf92cffb6e491ced97',
        creator='toby',
    )

    batch_id = str(batch['_id'])
    run_batch(batch_id)
    
    instagram_account = InstagramAccounts.create(
        id=instagram_id,
        username=instagram_username,
        image_url=instagram_image_url,
        access_token=access_token,
        company=company,
        batch_id=batch_id,
    )

    tokens_loader.store_token(
        'instagram',
        instagram_id,
        instagram_username,
        {
            'access_token': access_token
        },
    )

    send_to_channel(
        '{} authorized instagram for @{}.'.format(
            g.current_user.email,
            instagram_username,
        )
    )

    flash('Instagram connect success.', 'success')
    return redirect(
        url_for(
            'show_instagram_account',
            instagram_account_id=instagram_id,
        )
    )


@app.route('/instagram_accounts/<instagram_account_id>')
@auth_required
def show_instagram_account(instagram_account_id):
    try:
        instagram_account = InstagramAccounts.get(id=instagram_account_id)
    except InstagramAccounts.DoesNotExist:
        flash('That account does not exist.', 'error')
        return redirect(url_for('list_instagram_accounts'))

    if instagram_account.company != g.current_user.company:
        flash('That account does not exist.', 'error')
        return redirect(url_for('list_instagram_accounts'))

    batch = Batches.get_from_id(instagram_account.batch_id)
    breakdown = batch.get_breakdown()
    complete = batch['complete']
    num_users = batch.get_num_users()
    progress = (100 * num_users) / batch['limit']

    breakdown['filtered_interests'] = {}
    breakdown['traits'] = []
    breakdown['brands'] = []
    breakdown['shows'] = []
    breakdown['publishers'] = []
    hierarchy = []

    if complete:
        for index, row in enumerate(breakdown['interests']):
            interest_id, count, baselined_count = row
            interest = interests_loader.get_from_id(interest_id)
            percentage = count / float(num_users)

            if count <= 0:
                continue

            if interest['category'] in {'trait', 'brand', 'show', 'publisher'}:
                interest_category = interest['category'] + 's'
                breakdown[interest_category].append((interest, percentage))
            else:
                percentage = round(100 * percentage, 1)
                breakdown['filtered_interests'][interest_id] = (interest, percentage)

        breakdown['country'] = [
            (country, count)
            for country, count, baselined_count in breakdown['country']
        ]

	breakdown['country'] = sorted(
            breakdown['country'],
            key=lambda x: x[1],
            reverse=True
        )

        breakdown['gender'] = [
            (gender, count / float(num_users))
            for gender, count, baselined_count in breakdown['gender']
            if count > 0
        ]

        def child_in_hierarchy(target_ids, interest, top_level=False):
            if not top_level and interest['key'] in target_ids:
                return True
            if not len(interest['children']):
                return False
            return any(
                child_in_hierarchy(target_ids, interest)
                for interest in interest['children']
            )

        for interest in interests_loader.HIERARCHY:
            if child_in_hierarchy(breakdown['filtered_interests'], interest, top_level=True):
                hierarchy.append(interest)

        for breakdown_type in breakdown:
            if breakdown_type == 'country':
                continue

            if breakdown_type == 'filtered_interests':
                continue

            breakdown[breakdown_type] = [
                (r[0], round(100 * r[1], 1))
                for r in breakdown[breakdown_type]
                if breakdown_type == 'gender' or
                   (r[0] != '?' and r[1] >= 0.05)
            ]

    return render_template(
        'instagram_accounts/show.html', 
        instagram_account=instagram_account,
        breakdown=breakdown,
        progress=progress,
        complete=complete,
        hierarchy=hierarchy,
    )
