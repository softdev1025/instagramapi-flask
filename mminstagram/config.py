import os

from flask import Flask

os.environ['SLACK']
os.environ['SENSUS_PASSWORD']
os.environ['INSTAGRAM_CLIENT']
os.environ['INSTAGRAM_SECRET']
os.environ['DB_NAME']
os.environ['DB_USER']
os.environ['DB_PASS']
os.environ['DB_HOST']
os.environ['APP_SECRET']

dirname, filename = os.path.split(os.path.abspath(__file__))
template_dir = os.path.join(dirname, 'templates')
static_dir = os.path.join(dirname, 'static')

app = Flask(
    'mm-instagram',
    template_folder=template_dir,
    static_folder=static_dir,
)
app.secret_key = os.environ['APP_SECRET']
