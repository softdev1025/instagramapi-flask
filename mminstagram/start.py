import argparse
import logging

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from mminstagram.config import app
import mminstagram.csrf
import mminstagram.utils
from mminstagram.views import instagram_accounts, pages, \
    user_registrations, user_sessions


parser = argparse.ArgumentParser()
parser.add_argument('port', type=int)
parser.add_argument('--host', default='127.0.0.1')
args = parser.parse_args()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
if logger.handlers:
    handler = logger.handlers[0]
else:
    handler = logging.StreamHandler()
    logger.addHandler(handler)

handler.setFormatter(logging.Formatter('%(asctime)s || %(message)s'))

logging.info("Running on http://%s:%d" % (args.host, args.port))

http_server = HTTPServer(WSGIContainer(app))
http_server.listen(args.port, address=args.host)
IOLoop.instance().start()
