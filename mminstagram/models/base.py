import os

from peewee import Model
from playhouse.postgres_ext import PostgresqlExtDatabase

db = PostgresqlExtDatabase(
    os.environ['DB_NAME'],
    user=os.environ['DB_USER'],
    password=os.environ['DB_PASS'],
    host=os.environ['DB_HOST'],
    register_hstore=False,
)

class BaseModel(Model):
    class Meta:
        database = db
