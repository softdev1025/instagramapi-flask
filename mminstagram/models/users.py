from peewee import TextField, ForeignKeyField
from base import BaseModel
from companies import Companies

class Users(BaseModel):
    name = TextField()
    email = TextField(unique=True)

    salt = TextField()
    salted_hashed_password = TextField()
    auth_token = TextField()

    company = ForeignKeyField(
        Companies,
        related_name='users',
    )
