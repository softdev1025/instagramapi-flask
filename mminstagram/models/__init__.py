from base import db
from users import Users
from companies import Companies
from instagram_accounts import InstagramAccounts

db.connect()

db.create_tables([
    Users,
    Companies,
    InstagramAccounts,
], safe=True)

db.close()
