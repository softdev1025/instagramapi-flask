from peewee import TextField

from base import BaseModel

class Companies(BaseModel):
    name = TextField()
