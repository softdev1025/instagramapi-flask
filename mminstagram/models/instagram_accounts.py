from datetime import datetime

from peewee import TextField, ForeignKeyField, DateTimeField

from base import BaseModel
from companies import Companies

class InstagramAccounts(BaseModel):
    id = TextField(primary_key=True)

    username = TextField()
    access_token = TextField()
    image_url = TextField()

    batch_id = TextField()

    company = ForeignKeyField(
        Companies,
        related_name='instagram_accounts',
    )

    updated = DateTimeField(default=datetime.now)
