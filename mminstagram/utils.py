from datetime import datetime
from functools import wraps
import os

import celery
from flask import g, redirect, request, session, url_for, flash
from instagram.client import InstagramAPI
import humanize

from mminstagram.config import app
from mminstagram.models import db, Users, InstagramAccounts


CELERY = celery.Celery()
CELERY.config_from_object('mmsensus.celeryconfig.batches')


def instagram_client():
    return InstagramAPI(
        client_id=os.environ['INSTAGRAM_CLIENT'],
        client_secret=os.environ['INSTAGRAM_SECRET'],
        redirect_uri=url_for(
            'authorize_instagram_account',
            _external=True,
            _scheme='https',
        )
    )


def create_user_auth_session(user_id, auth_token):
    session['user_id'] = user_id
    session['auth_token'] = auth_token


def destroy_user_auth_session():
    g.current_user = None

    if 'user_id' in session:
        del session['user_id']

    if 'auth_token' in session:
        del session['auth_token']


def get_user_from_session():
    if 'user_id' not in session:
        return destroy_user_auth_session

    if 'auth_token' not in session:
        return destroy_user_auth_session()

    user_id = session['user_id']
    auth_token = session['auth_token']

    if not len(auth_token) or not user_id:
        return destroy_user_auth_session

    try:
        user = Users.get(id=user_id)
        if user.auth_token != auth_token:
            return destroy_user_auth_session()

        g.current_user = user

    except Users.DoesNotExist:
        return destroy_user_auth_session()


def auth_required(f):
    @wraps(f)
    def dec(*args, **kwargs):
        if g.current_user:
            return f(*args, **kwargs)
        return redirect(url_for('new_user_session'))
    return dec


def noauth_required(f):
    @wraps(f)
    def dec(*args, **kwargs):
        if g.current_user:
            return redirect('/')
        return f(*args, **kwargs)
    return dec


@app.before_request
def before():
    g.current_user = None
    g.db = db
    g.db.connect
    get_user_from_session()


@app.after_request
def after(response):
    g.db.close()
    return response


def run_batch(batch_id):
    CELERY.send_task('run_batch', [batch_id])


@app.template_filter('human_since')
def humanize_since(date):
    return humanize.naturalday(date)


@app.template_filter('human_time')
def humanize_time(date):
    return humanize.naturaltime(date)
    
