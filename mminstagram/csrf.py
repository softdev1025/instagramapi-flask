import os
import binascii

from flask import abort, g, request, session

from mminstagram.config import app


@app.before_request
def csrf_token():
    if request.method == 'POST':
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)


def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = binascii.hexlify(os.urandom(32))

    return session['_csrf_token']


app.jinja_env.globals['csrf_token'] = generate_csrf_token
