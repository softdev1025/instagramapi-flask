$(document).ready(function() {
    $("[data-popup][data-content]").popup({
        variations: "inverted",
    });
    $(".ui.message .close.icon").click(function() {
        $(this).closest(".ui.message").hide();
    });
});
